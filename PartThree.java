import java.util.Scanner;
 public class PartThree {
  public static void main (String[] args){
    Scanner reader = new Scanner(System.in);
    System.out.println("Enter the length side of the square");
    double lengthSide = reader.nextDouble();
    System.out.println("Enter the height of the rectangle");
    double height = reader.nextDouble();
    System.out.println("Enter the width of the rectangle");
    double width = reader.nextDouble();
    
    System.out.println("The area of square is: " + AreaComputations.areaSquare(lengthSide));
    
    AreaComputations ar = new AreaComputations();
    System.out.println("The area of square is: " + ar.areaRectangle(height,width));
  }
 }
    