public class MethodTest {
  public static void main (String[] args) {
    int x =10;
    methodNoInputNoReturn();
    System.out.println(x);
    methodOneInputNoReturn(10);
    methodOneInputNoReturn(x);
    methodOneInputNoReturn(x+50);
    methodTwoInputNoReturn(12,1.2);
    int z = methodNoInputReturnInt();
    System.out.println(z);
    double sqrt = sumSquareRoot(3,6);
    System.out.println(sqrt);
    String s1 = "hello";
    String s2 = "goodbye";
    System.out.println(s1.length());
    System.out.println(s2.length());
    System.out.println(SecondClass.addOne(50));
    SecondClass sc = new SecondClass();
    System.out.println(sc.addTwo(50));
  }
  
  public static void methodNoInputNoReturn() {
    System.out.println("I’m in a method that takes no input and returns nothing");
    int x =50;
    System.out.println(x);
  }
  public static void methodOneInputNoReturn (int a) {
   System.out.println("Inside the method one input no return");
   System.out.println(a);
  }
  public static void methodTwoInputNoReturn (int a, double b) {
    System.out.println("Inside the method two input no return");
    System.out.println(a);
    System.out.println(b);
  }
  public static int methodNoInputReturnInt() {
   int x =6;
   return x;
  }
  public static double sumSquareRoot(int a , int b){
   int sum = a+b;
   double sqrt = Math.sqrt(sum);
   return sqrt;
  }
}